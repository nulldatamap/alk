﻿using System.Text;
using Veldrid;
using Veldrid.Sdl2;
using Veldrid.StartupUtilities;
using Vulkan;

public abstract class AlkValue
{
    public static readonly AlkConstant Null = new AlkConstant("null");
    public static readonly AlkConstant True = new AlkConstant("true");
    public static readonly AlkConstant False = new AlkConstant("false");
    public static readonly AlkConstant Mark = new AlkConstant("mark");

    public abstract bool IsFrozen { get; }
    public abstract void Freeze();
}

public abstract class AlkAtom : AlkValue
{
    public override bool IsFrozen => true;

    public override void Freeze()
    {
    }
}

public sealed class AlkConstant : AlkAtom
{
    private readonly string _displayName;

    internal AlkConstant(string n)
    {
        _displayName = n;
    }

    public override string ToString() => _displayName;
}

public sealed class AlkBuiltin : AlkAtom
{
    private Action<Alk> _builtin;
    private string _name;

    public AlkBuiltin(string name, Action<Alk> act)
    {
        _builtin = act;
        _name = name;
    }

    public void Exec(Alk rt) => _builtin(rt);

    public override string ToString() => _name;
}

public sealed class AlkInt : AlkAtom
{
    public readonly long Value;

    public static explicit operator long(AlkInt i) => i.Value;

    public AlkInt(long v)
    {
        Value = v;
    }

    public override string ToString() => Value.ToString();
}

public sealed class AlkArray : AlkValue
{
    private readonly List<AlkValue> _values;

    internal bool _isFrozen = false;
    public override bool IsFrozen => _isFrozen;

    public override void Freeze()
    {
        foreach (var v in _values)
        {
            if (!v.IsFrozen)
                v.Freeze();
        }

        _isFrozen = true;
    }

    public AlkArray()
    {
        _values = new List<AlkValue>();
    }

    public AlkValue Get(int idx) => _values[idx];

    public void Set(int idx, AlkValue v)
    {
        if (_isFrozen)
            throw new InvalidOperationException($"Can't `set` on a frozen array");

        _values[idx] = v;
    }

    public void Push(AlkValue v)
    {
        if (_isFrozen)
            throw new InvalidOperationException($"Can't `push` on a frozen array");

        _values.Add(v);
    }

    public int Count => _values.Count;

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("[");
        foreach (var v in _values)
        {
            sb.Append(" ");
            sb.Append(v);
        }
        sb.Append(" ]");

        return sb.ToString();
    }
}
public sealed class AlkDict : AlkValue
{
    private readonly Dictionary<AlkValue, AlkValue> _dict;
    internal bool _isFrozen = false;

    public override bool IsFrozen => _isFrozen;

    public override void Freeze()
    {
        foreach (var kv in _dict)
        {
            if (!kv.Key.IsFrozen)
                kv.Key.Freeze();
            if (!kv.Value.IsFrozen)
                kv.Value.Freeze();
        }

        _isFrozen = true;
    }

    public AlkDict()
    {
        _dict = new Dictionary<AlkValue, AlkValue>();
    }

    public bool TryLookup(AlkValue k, out AlkValue v)
    {
        Console.WriteLine($"{k} ~~ {k.GetHashCode()}");
        return _dict.TryGetValue(k, out v);
    }

    public void Put(AlkValue k, AlkValue v)
    {
        if (_isFrozen)
            throw new InvalidOperationException($"Can't `put` on a frozen dict");

        _dict[k] = v;
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<<");
        foreach (var kv in _dict)
        {
            sb.Append(" ");
            sb.Append(kv.Key);
            sb.Append(" ");
            sb.Append(kv.Value);
        }
        sb.Append(" >>");

        return sb.ToString();
    }
}

public abstract class AlkWord : AlkAtom
{
    public readonly string Name;

    public AlkWord(string n)
    {
        Name = n;
        Console.WriteLine($"{n} ~~ {n.GetHashCode()}");
    }

    public override bool Equals(object? obj)
    {
        if (obj == null)
            return false;

        if (obj is AlkWord w)
            return w.Name == Name;

        return false;
    }

    public override int GetHashCode()
    {
        return Name.GetHashCode();
    }
}

public sealed class AlkKeyword : AlkWord
{
    public AlkKeyword(string n) : base(n) {}

    public override string ToString() => $"/{Name}";
}

public sealed class AlkSymbol : AlkWord
{
    public AlkSymbol(string n) : base(n) {}
    public override string ToString() => Name;
}

public class Alk
{
    private static readonly AlkDict _builtins = new AlkDict();

    private static readonly (string, Action<Alk>)[] _builtinDefs = new (string, Action<Alk>)[]
    {
        ("add", (Alk rt) => throw new InvalidOperationException($"add!")),
    };

    static Alk()
    {
        foreach (var (k, v) in _builtinDefs)
        {
            _builtins.Put(new AlkSymbol(k), new AlkBuiltin(k, v));
        }

        _builtins.Freeze();
    }

    internal Stack<AlkValue> _execution;
    internal Stack<AlkValue> _operands;
    internal Stack<AlkDict> _environments;

    public Alk()
    {
        _execution = new Stack<AlkValue>();
        _operands = new Stack<AlkValue>();
        _environments = new Stack<AlkDict>();

        _environments.Push(_builtins);
        _environments.Push(new AlkDict());
    }

    public bool Step()
    {
        if (_execution.Count == 0)
            return false;

        if (_execution.Count % 2 != 0)
            throw new InvalidOperationException($"Invalid execution stack: uneven number of elements");

        var idxV = _execution.Pop();
        var cntV = _execution.Pop();
        if (idxV is not AlkInt idx)
            throw new InvalidCastException($"Invalid execution stack: expected index");

        switch (cntV)
        {
            case AlkBuiltin bi:
                bi.Exec(this);
                break;
            case AlkSymbol kw:
                var v = Resolve(kw);
                _execution.Push(v);
                _execution.Push(new AlkInt(0));
                break;
            case AlkArray arr:
                if (idx.Value == arr.Count)
                    break;

                _execution.Push(arr);
                _execution.Push(new AlkInt(idx.Value + 1));

                _execution.Push(arr.Get((int)idx.Value));
                _execution.Push(new AlkInt(0));

                break;
            default:
                _operands.Push(cntV);
                break;
        }

        return true;
    }

    private AlkValue Resolve(AlkWord w)
    {
        foreach (var env in _environments)
        {
            if (env.TryLookup(w, out var v))
            {
                return v;
            }
        }

        throw new InvalidOperationException($"Failed to resolve: {w}");
    }

    public void Debug()
    {
        Console.WriteLine($"== STATE:");
        Console.Write($"= OPERANDS: ");
        foreach (var v in _operands)
        {
            Console.Write(v);
            Console.Write(" ");
        }
        Console.WriteLine();
        Console.Write($"= EXECUTION: ");
        foreach (var v in _execution)
        {
            Console.Write(v);
            Console.Write(" ");
        }
        Console.WriteLine();
        Console.WriteLine($"= ENVIRONMENTS: ");
        foreach (var env in _environments)
        {
            Console.WriteLine($"- {env}");
        }
    }
}

public class Program
{
    private static Sdl2Window _window;
    private static GraphicsDevice _gd;
    private static ImGuiRenderer _controller;
    private static CommandList _cl;


    public static void Main()
    {
        var program = new AlkArray();
        program.Push(new AlkInt(3));
        program.Push(new AlkInt(2));
        program.Push(new AlkSymbol("add"));

        var alk = new Alk();
        alk._execution.Push(program);
        alk._execution.Push(new AlkInt(0));

        alk.Debug();
        while (alk.Step())
        {
            alk.Debug();
        }


        new Program().Run();
    }

    public void Run()
    {
        InitGUI();

        while (_window.Exists)
        {
            InputSnapshot snapshot = _window.PumpEvents();
            if (!_window.Exists)
            {
                break;
            }

            _controller.Update(1 / 60f, snapshot);

            _cl.Begin();
            _cl.SetFramebuffer(_gd.MainSwapchain.Framebuffer);
            _cl.ClearColorTarget(0, new RgbaFloat(0, 0, 0, 1));
            _controller.Render(_gd, _cl);
            _cl.End();
            _gd.SubmitCommands(_cl);
            _gd.SwapBuffers(_gd.MainSwapchain);
        }

        _gd.WaitForIdle();
        _controller.Dispose();
        _cl.Dispose();
        _gd.Dispose();
    }

    private static void InitGUI()
    {
        VeldridStartup.CreateWindowAndGraphicsDevice(
            new WindowCreateInfo(50, 50, 800, 600, WindowState.Normal, "Alk"),
            new GraphicsDeviceOptions(true, null, true, ResourceBindingModel.Improved, true, true),
            out _window,
            out _gd);

        _window.Resized += () =>
        {
            _gd.MainSwapchain.Resize((uint)_window.Width, (uint)_window.Height);
            _controller.WindowResized(_window.Width, _window.Height);
        };
        _cl = _gd.ResourceFactory.CreateCommandList();
        _controller = new ImGuiRenderer(_gd, _gd.MainSwapchain.Framebuffer.OutputDescription, _window.Width,
            _window.Height);
    }
}